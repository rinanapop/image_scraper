import configparser
import os
import requests
import sys
from bs4 import BeautifulSoup as btsp

config_ini = configparser.ConfigParser()
config_ini.read('config.ini', encoding='utf-8')

URL = sys.argv[1]
BASE_DIR = config_ini['DEFAULT']['base_dir']
SITE_NAME = URL.split('/')[2]
ALBUM_NAME = URL.split('/')[-2]
SAVE_DIR = BASE_DIR + SITE_NAME + '/'
ALBUM_DIR = SAVE_DIR + ALBUM_NAME + '/'

def is_yes(ques):
    while True:
        ans = input(f'{ques}[y/n]: ').lower()
        if ans in ['y', 'ye', 'yes']:
            return True
        elif ans in ['n', 'no']:
            return False

def exists_album():

    if not os.path.exists(ALBUM_DIR):
        os.makedirs(ALBUM_DIR)
        print(f'python generated directory "{ALBUM_DIR}"')
        return True

    else:
        print(f'You already have a directory "{ALBUM_DIR}"')
        return is_yes('Would you like to continue?: ')


def exists_url(url):
    if not config_ini['DEFAULT'].getboolean('overwrite'):
        with open('source_link.txt', 'r') as links:
            link_lines = links.read().split('\n')
            return url in link_lines

def main():

    if not exists_url(URL) and exists_album():
            
        res = requests.get(URL)
        soup = btsp(res.text, 'lxml')

        img_links = soup.find_all(class_='msacwl-img-link')

        for i in img_links:
            img_link = i.get('data-mfp-src')
            img_re = requests.get(img_link)
            file_name = img_link.split('/')[-1]

            with open(ALBUM_DIR + file_name, 'wb') as f:
                f.write(img_re.content)
            print(f'successful save {SAVE_DIR + file_name}')

        with open('source_link.txt', 'a') as f:
            f.write(URL + '\n')

    else:
        print('You already downloaded pictures in this link')
        print(URL)

if __name__ == "__main__":
    main()
